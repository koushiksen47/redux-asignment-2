import React, { Component } from "react";
import "./AddPerson.css";
class AddPerson extends Component {
  state = { name: null, age: null };
  onChangeName = event => {
    this.setState({ name: event.target.value });
  };
  onChangeAge = event => {
    this.setState({ age: event.target.value });
  };
  render() {
    return (
      <div className="AddPerson">
        <input
          placeholder="Enter name"
          value={this.state.name}
          onChange={this.onChangeName}
        />
        <input
          placeholder="Enter age"
          value={this.state.age}
          onChange={this.onChangeAge}
        />
        <button
          onClick={() =>
            this.props.personAdded(this.state.name, this.state.age)
          }
        >
          Add Person
        </button>
      </div>
    );
  }
}

export default AddPerson;
