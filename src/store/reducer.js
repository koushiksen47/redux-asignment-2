const initialState = {
  persons: []
};

const reducer = (state = initialState, actions) => {
  if (actions.type === "ADD") {
    const newPerson = {
      id: Math.random(), // not really unique but good enough here!
      name: actions.personData.name,
      age: actions.personData.age
    };
    return { persons: state.persons.concat(newPerson) };
  } else if (actions.type === "DELETE") {
    return {
      persons: state.persons.filter(person => person.id !== actions.personId)
    };
  }
  return state;
};
export default reducer;
